File Name, {strReportFileName}+ ".CSV"
Date, {Date}
Time, {Time}

Project Start Time:, {strGAutoRunStartTime} 
Project Stop Time:, {strGAutoRunEndTime}

**Conveyors**,
Conveyor Speed (mm/s),     {TopConv.reHAutoSPD} 		
Conveyor Batch Lenght(mm), {topconv.reBATCHlngSP}		
Conveyor Start SP	,      {topconv.reBATCHlngSP }									
Conveyor Pause SP	,      {Topconv.inBUFFlvlSTPsp}		
Conveyor Control SD	, 	{TopConv.inBUFFcntrlSDsp}	
Conveyor Total Batch Processed, {inGhTotalBATCHcnt - TopConv.inBUFFcntrlSDsp}

**Water Tank Temperature**
Minimum Temperature,{Tank1.inMinTemp}
Maximum Temperature,{Tank1.inMaxTemp}


Power Supplies,
Power Supply #, Enabled, Command,
1,{if(LCR[1].boHenb and LCR[1].boHAutoMan, "True","False")},{LCRpsCom[1].inHAutoPWRcmd} mA 
2,{if(LCR[2].boHenb and LCR[2].boHAutoMan, "True","False")},{LCRpsCom[2].inHAutoPWRcmd} mA 
3,{if(LCR[3].boHenb and LCR[3].boHAutoMan, "True","False")},{LCRpsCom[3].inHAutoPWRcmd} mA 
4,{if(LCR[4].boHenb and LCR[4].boHAutoMan, "True","False")},{LCRpsCom[4].inHAutoPWRcmd} mA 
5,{if(LCR[5].boHenb and LCR[5].boHAutoMan, "True","False")},{LCRpsCom[5].inHAutoPWRcmd} mA 
6,{if(LCR[6].boHenb and LCR[6].boHAutoMan, "True","False")},{LCRpsCom[6].inHAutoPWRcmd} mA 
7,{if(LCR[7].boHenb and LCR[7].boHAutoMan, "True","False")},{LCRpsCom[7].inHAutoPWRcmd} mA 
8,{if(LCR[8].boHenb and LCR[8].boHAutoMan, "True","False")},{LCRpsCom[8].inHAutoPWRcmd} mA 
9,{if(LCR[9].boHenb and LCR[9].boHAutoMan, "True","False")},{LCRpsCom[9].inHAutoPWRcmd} mA 
10,{if(LCR[10].boHenb and LCR[10].boHAutoMan, "True","False")},{LCRpsCom[10].inHAutoPWRcmd} mA 




